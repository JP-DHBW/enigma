TEMPLATE = app
TARGET = Enigma

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

HEADERS += enigma.h logger.h \
    gui.h
SOURCES += main.cpp logger.cpp decode.cpp encode.cpp gui.cpp inputparse.cpp keyparse.cpp plugs.cpp rotors.cpp enigma.cpp


FORMS += gui.ui \
    gui.ui

