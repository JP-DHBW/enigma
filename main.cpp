using namespace std;
//Main File
//Start point
#include "enigma.h"
#include <QApplication>
#include <gui.h>
#include "tests.cpp"
#include "logger.h"


int main(int argc, char **argv){

    QApplication enigma(argc, argv);
    Gui enigmaWindow;
    enigmaWindow.show();

    return enigma.exec();

    //===========|Change comment for TEST|===========

    //TestSingleLetterEncode();
    //return 0;
}
