#include "enigma.h"

void Machine::writeToFile(string filePath, string data){
    std::ofstream file(filePath);
    file << data << std::endl;
}

string Machine::readFile(string filePath){
    //Methode um eine Datei Auszulesen
    //Parameter: filePath | string, welcher die zu lesende Datei angibt
    //Ausgabe  : Inhalt der Datei als string

    ifstream file(filePath);

    if(!file){
        //Datei nicht gefunden / nicht vorhanden
        Output::Logger::logError("File not found!");
        return NULL;
    }

    stringstream buffer;

    buffer << file.rdbuf();

    string data = buffer.str();

    //Dateilänge speichern -> UI/Progressbar
    this->fileLength = data.length();

    return data;
}

string Machine::removeFileExtension(string filePath){
    //Methode um die Dateiendung zu entfernen
    //Bsp: test.txt => test
    //Parameter: filePath | string, welcher den zu ändernden Pfad angibt
    //Ausgabe  : filePath ohne Dateikürzel

    size_t lastindex = filePath.find_last_of(".");
    string file = filePath.substr(0, lastindex);
    return file;
}
