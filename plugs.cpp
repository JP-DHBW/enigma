#include "enigma.h"

char Machine::runPlugs(char letter){
    //Methode um ein Character durch alle Plugs laufen zu lassen
    //Wenn Plug A mit T verbunden ist, gibt der Buchstabe A T aus und T A.
    //Parameter: letter | character des zu ändernden Buchstabens
    //Ausgabe  : geänderter Buchstabe als char

    for (int i=0; i < 10; i++){
        int letnmb = Machine::getAlphabetPosition(letter);
        if (letnmb==this->plugs[i][0]){
            letnmb = this->plugs[i][1];
            return Machine::getLetter(letnmb);
        }
        else if(letnmb==this->plugs[i][1]){
            letnmb = this->plugs[i][0];
            return Machine::getLetter(letnmb);
        }
    }
    return letter;
}

bool Machine::isNotPlugged(int nmbr){
    //Funktion um zu ermitteln ob folgende Verbindung bereits Besteht
    //Parameter: nmbr | Nummer des characters im Alphabet
    //Ausgabe  : bool, wenn false: verbindung vorhanden, wenn true nicht.

    for (int i = 0; i < 10; i++){
        if (this->plugs[i][0] == nmbr || this->plugs[i][1] == nmbr){
            return false;
        }
    }
    return true;
}
