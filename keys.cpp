#include "enigma.h"


void Machine::createRandomKey(){
    srand(time(0));
    //rotors
    this->rotors[0] = rand()%8;
    int rnd = rand()%8;
    while (this->rotors[0] == rnd){
        rnd = rand()%8;
    }
    this->rotors[1] = rnd;
    int rnd2 = rand()%8;
    while (this->rotors[0] == rnd2 || this->rotors[1] == rnd2) {
        rnd2 = rand()%8;
    }
    this->rotors[2] = rnd2;
    for (int i = 0; i < 10; i++){
        int nmbr = rand()%26;
        while (!this->isNotPlugged(nmbr)){
            nmbr = rand()%26;
        }
        this->plugs[i][0] = nmbr;
        int nmbr2 = rand()%26;
        while (!this->isNotPlugged(nmbr2)){
            nmbr2 = rand()%26;
        }
        this->plugs[i][1] = nmbr2;
    }
}


