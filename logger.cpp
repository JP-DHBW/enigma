#include "logger.h"
#include <iostream>
#include <string>
using namespace std;
using namespace Output;

void Logger::log(string input){
  /*Usage:
  *Logger::log("string");
  *Logs Data in Format: [INFO] string
  */
  cout << "[INFO] " << input << endl;
}

void Logger::logWarning(string input){
  /*Usage:
  *Logger::logWarning("string");
  *Logs Data in Format: [INFO] [WARNING] string
  */
  Logger::log("[WARNING] " + input);
}

void Logger::logError(string input){
  /*Usage:
  *Logger::logError("string");
  *Logs Data in Format: [INFO] [ERROR] string
  */
  Logger::log("[ERROR] " + input);
}

void Logger::logDebug(string input){
  /*Usage:
  *Logger::logError("string");
  *Logs Data in Format: [INFO] [DEBUG] string
  */
  Logger::log("[DEBUG] " + input);
}
