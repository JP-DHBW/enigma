#include "enigma.h"

//Tests für ENIGMA!
//Wird eh nicht klappen
//HOFFnung stirbt zuletzt
//aber stirbt
static void TestSingleLetterEncode(){
    //Testet encode und decode von einem Letter
    int plugs[10][2];
    int rotor[3];
    for(int i = 0; i <10; i++){
        plugs[i][0] = 26;
        plugs[i][1] = 26;
    }
    rotor[0] = 0;
    rotor[1] = 1;
    rotor[2] = 2;
    Machine m = Machine("/home/joey/github/enigma/testfiles/gui.txt", plugs, rotor);
    //m.encode();
    string encoded = m.readFile("/home/joey/github/enigma/testfiles/gui.txt.enigma");
    Output::Logger::log("Output: " + encoded);
    //Decode
    Machine dm = Machine("/home/joey/github/enigma/testfiles/gui.txt.key", "/home/joey/github/enigma/testfiles/gui.txt.enigma");
    //dm.decode();
    string decode = dm.readFile("/home/joey/github/enigma/testfiles/gui.txt.enigma.enigma");
    Output::Logger::log("Decoded Output: " + decode);
}
