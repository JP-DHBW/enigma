#include "enigma.h"

/*
 * Syntax for Key:
 * posRot1,posRot2,posRot3:rot1,rot2,rot3:plug[0][0].plug[0][1],plug[......
 */
void Machine::createKey(){
    string key = this->key;
    string output = "";
    output += key + ":";
    output += std::to_string(this->rotors[0]) + ",";
    output += std::to_string(this->rotors[1]) + ",";
    output += std::to_string(this->rotors[2]) + ":";
    for (int i = 0; i < 10; i++){
        output += std::to_string(this->plugs[i][0]) + ".";
        output += std::to_string(this->plugs[i][1]) + ",";
    }
    Machine::writeToFile(this->filePath+".key", output);
}

vector<string> split(string input, char regex){
    std::replace(input.begin(), input.end(), regex, ' ');
    stringstream ss(input);
    string tmp;
    vector<string> output;
    while (ss >> tmp){
        output.push_back(string(tmp));
    }
    return output;
}

void Machine::loadKey(){
    //Loads Key Values
    string data = Machine::readFile(this->key);
    vector<string> info = split(data, ':');
    vector<string> offset = split(info[0], ',');
    vector<string> rotors = split(info[1], ',');
    vector<string> plugs = split(info[2], ',');

    this->posRot1 = std::stoi(offset[0]);
    this->posRot2 = std::stoi(offset[1]);
    this->posRot3 = std::stoi(offset[2]);

    this->rotors[0] = std::stoi(rotors[0]);
    this->rotors[1] = std::stoi(rotors[1]);
    this->rotors[2] = std::stoi(rotors[2]);

    for (int i = 0; i < 10; i++){
        vector<string> plugData = split(plugs[i], '.');
        this->plugs[i][0] = std::stoi(plugData[0]);
        this->plugs[i][1] = std::stoi(plugData[1]);
    }
}
