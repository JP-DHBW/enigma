#include "enigma.h"

void Machine::rotate(int i){
    //Methode um die Rotoren um einen Schritt zu ehöhen
    //Bsp: Aufzählung auf Basis 26
    //Parameter: i | integer welcher angibt, wie viele schritte bereits gelaufen sind
    //Keine Ausgabe.
    this->posRot1++;
    if (this->posRot1%26==0){
        this->posRot1 =0;
        this->posRot2++;
        if (this->posRot2%26==0){
            this->posRot2 = 0;
            this->posRot3++;
            if (this->posRot3%26==0){
                this->posRot3 = 0;
            }
        }
    }
}

char Machine::rotateLetter(int nmb, char letter){
    //Methode um einen Buchstaben durch einen Rotoren laufen zu lassen
    //Parameter: nmb, letter | Integer, welcher den Rotor angibt und char welcher den zu rotierenden Buchstaben angibt
    //Ausgabe  : character der
    int offset = 0;
    switch (nmb) {
    case 0:
        offset = this->posRot1;
        break;
    case 1:
        offset = this->posRot2;
        break;
    case 2:
        offset = this->posRot3;
        break;
    default:
        break;
    }
    int pos = (Machine::getAlphabetPosition(letter) + offset)%26;
    int rot = this->rotors[nmb];
    switch (rot) {
    case 0:
        letter = rotor_0[pos];
        break;
    case 1:
        letter = rotor_1[pos];
        break;
    case 2:
        letter = rotor_2[pos];
        break;
    case 3:
        letter = rotor_3[pos];
        break;
    case 4:
        letter = rotor_4[pos];
        break;
    case 5:
        letter = rotor_5[pos];
        break;
    case 6:
        letter = rotor_6[pos];
        break;
    case 7:
        letter = rotor_7[pos];
        break;
    default:
        break;
    }
    return letter;
}

char Machine::runRotors(char letter, int state){
    //Einstiegsmethode um einen Buchstaben durch alle ausgewählten Rotoren + reflector laufen zu lassen
    //Rekursiver aufbau
    //Parameter: letter, state | char welcher den Buchstaben angibt und Int welcher den status der Rekursion (=Anzahl durchläufe) angibt (!!BEIM AUFRUF IMMER 0!!)
    //Ausgabe  : Endausgabe ist der Letter, welcher durch alle Elemente gelaufen ist
    state++;
    if (state >= 4){
        if (state == 8){
            return letter;
        }
        else if (state==4) {
            letter = reflector[Machine::getAlphabetPosition(letter)];
            return this->runRotors(letter, state);
        }
        else{
            letter = rotateLetter(7-state, letter);
            return this->runRotors(letter, state);
        }
    }
    else {
        letter = rotateLetter(state-1, letter);
        return this->runRotors(letter, state);
    }
}

void Machine::createRandomRotPos(){
    //Erstellt zufällig generierte Position der Rotoren
    srand(time(0));
    int rt1 = (rand() % 26);
    int rt2 = (rand() % 26);
    int rt3 = (rand() % 26);
    this->posRot1 = rt1;
    this->posRot2 = rt2;
    this->posRot3 = rt3;

    //keydefinition missing
    Output::Logger::logDebug("First Rotor offset: " + std::to_string(rt1));

    Output::Logger::logDebug("Second Rotor offset: " + std::to_string(rt2));

    Output::Logger::logDebug("Third Rotor offset: " + std::to_string(rt3));
    this->key += std::to_string(rt1) + ",";
    this->key += std::to_string(rt2) + ",";
    this->key += std::to_string(rt3);
}
