#include "enigma.h"

bool Machine::isLowerCase(char letter){
    if (letter == toupper(letter)){
        return false;
    }
    return true;
}

void Machine::decode(){
    //Einstiegsmethode zum decode Prozess

    Output::Logger::log("Starting Decode of filePath:");
    Output::Logger::log(this->filePath);
    Output::Logger::log("KeyPath: ");
    Output::Logger::log(this->key);

    string data = Machine::readFile(this->filePath); //Datei auslesen
    this->filePath = Machine::removeFileExtension(this->filePath);

    string returnData = "";
    for (int i = 0; i < this->fileLength; i++){
        char tmpChar = data[i];
        bool toLower = false;

        QRegExp alphabet("[A-Za-z]");
        if (alphabet.exactMatch(QString(tmpChar))){
            if (Machine::isLowerCase(tmpChar)){
                toLower = true;
            }
            tmpChar = toupper(tmpChar);
            //======================[PLUG - 1]======================
            tmpChar = this->runPlugs(tmpChar);

            //======================[ROTOREN]======================
            tmpChar = this->reverseRotor(tmpChar, 0);
            this->rotate(i);

            //======================[PLUG - 2]======================
            tmpChar = this->runPlugs(tmpChar);

            if (toLower){
                tmpChar = tolower(char(tmpChar));
            }
        }
        returnData += tmpChar;
    }
    Machine::writeToFile(this->filePath + "_decode.txt", returnData);
    Output::Logger::log("Finished decode. Outputfile:");
    Output::Logger::log(this->filePath + "_decode.txt");
}
