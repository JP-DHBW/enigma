#ifndef GUI_H
#define GUI_H

#include <QMainWindow>
#include <enigma.h>

namespace Ui {
class Gui;
}

class Gui : public QMainWindow
{
    Q_OBJECT

public:
    explicit Gui(QWidget *parent = nullptr);

    //multidimensional array with plugs settings for the enigmamachine to encode
    int plugs[10][2];

    //array with rotorsettings for the enigmamachine to encode
    int rotors[3];

    ~Gui();

private slots:

    //Open fileselectorfor source data
    void on_fileSelect_clicked();

    //ui updater for the selected source file
    //@filePath *.enigma; *.txt
    void changeSelectedFileTitle(const QString filePath);

    //ui updater for the selected source keyfile
    //@filePath: *.key
    void changeSelectedKeyFileTitle(const QString filePath);

    //ui updater if the quit button was clicked
    //--- Exits the programm ---
    void on_quitBtn_clicked();

    //ui updater for the plugAmt slider
    //@value: new plug value
    void on_plugAmt_valueChanged(int value);

    //ui updater when the main setting changed
    //@index: 0==decode; 1==selectedKey; 2==rndKey
    void on_codingSet_currentIndexChanged(int index);

    //ui updater when the run button was clicked
    //disabels the ui and start the decoding/encoding process
    void on_runBtn_clicked();

    //ui updater so every rotor can only be used once within the setting boxes
    void on_rot1_currentIndexChanged(int index);
    void on_rot2_currentIndexChanged(int index);
    void on_rot3_currentIndexChanged(int index);

    //ui updater so every letter can only be used once within the setting boxes
    void on_plugSelect82_currentIndexChanged(int index);
    void on_plugSelect11_currentIndexChanged(int index);
    void on_plugSelect12_currentIndexChanged(int index);
    void on_plugSelect21_currentIndexChanged(int index);
    void on_plugSelect22_currentIndexChanged(int index);
    void on_plugSelect31_currentIndexChanged(int index);
    void on_plugSelect32_currentIndexChanged(int index);
    void on_plugSelect41_currentIndexChanged(int index);
    void on_plugSelect42_currentIndexChanged(int index);
    void on_plugSelect51_currentIndexChanged(int index);
    void on_plugSelect52_currentIndexChanged(int index);
    void on_plugSelect61_currentIndexChanged(int index);
    void on_plugSelect62_currentIndexChanged(int index);
    void on_plugSelect71_currentIndexChanged(int index);
    void on_plugSelect72_currentIndexChanged(int index);
    void on_plugSelect81_currentIndexChanged(int index);
    void on_plugSelect91_currentIndexChanged(int index);
    void on_plugSelect92_currentIndexChanged(int index);
    void on_plugSelect93_currentIndexChanged(int index);
    void on_plugSelect94_currentIndexChanged(int index);

    //ui updater to display the selected keyfile
    void on_keyFileSelect_clicked();

private:
    //validation of userinput
    bool checkInputFile(int callType);
    bool checkInput();

    Ui::Gui *ui;
};

#endif // GUI_H
