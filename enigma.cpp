#include <iostream>
#include "enigma.h"

//callType: 0==decode, 1==selectedKey, 2==rndKey
//0
Machine::Machine(string inputKey, string inputFilePath){
    this->key = inputKey;
    this->loadKey();
    this->filePath = inputFilePath;
    this->decode();
}
//1
Machine::Machine(string inputFilePath, int inputPlugs[10][2],int inputRotors[3]){
    Machine::createRandomRotPos();
    this->filePath = inputFilePath;
    this->setArrays(inputPlugs, inputRotors);
    this->encode();
}
//2
Machine::Machine(string inputFilePath){
    this->createRandomKey();
    this->createRandomRotPos();
    this->filePath = inputFilePath;
    this->encode();
};
Machine::Machine(){};
Machine::~Machine(){};

void Machine::setArrays(int inputPlugs[10][2], int inputRotors[3]){
    //Setzt die Arrays, da c++ arr1 = arr2 nicht mag..
    //Arraycopy?? Was ist das?
    for (int i = 0; i < 10; i++){
        this->plugs[i][0] = inputPlugs[i][0];
        this->plugs[i][1] = inputPlugs[i][1];
    }
    for (int i = 0; i < 3; i++){
        this->rotors[i] = inputRotors[i];
    }
}

void Machine::setZeroOffset(){
    this->posRot1 = 0;
    this->posRot2 = 0;
    this->posRot3 = 0;
    this->key = "0,0,0";
}

