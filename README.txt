﻿Willkommen bei Enigma_DHBW

Dieses Programm wurde für das Programmieren-Projekt des Kurses TIM18 entwickelt.
Präsentationsdatum: 14.06.2019

==========================================[Compilieren]==========================================
Um das Programm Local zu compilieren, wird die Qt-Libary benötigt.
Source: https://www.qt.io/download

Um eine makefile zu generieren, muss abhängig vom OS/Betriebssystem ein bash/shell - Command in dem
Projektordner ausgeführt werden:

Linux: qmake -o Makefile enigma.pro

Windows: qmake -o Makefile -spec win32-g++ enigma.pro

MacOS: qmake -o Makefile -spec macx-g++ enigma.pro

==========================================[Programm]==========================================
Wenn man das Programm nutzt, dann öffnet sich zu Beginn eine Ui, in welcher man
zunächst den gewünschten Bearbeitungsvorgang auswählen kann.

3 Vorgänge stehen zur verfügung:

==========================================[Decode]==========================================
Zum Decodieren wird eine Key-Datei (Dateiendung: .key) benötigt, welche den Schlüssel beinhaltet.
Zuzüglich wird die zugehörige Codierte Datei (Dateiendung: .enigma) benötigt.

Wenn beide Dateien ausgewählt wurden, kann der "Run" Button betätigt werden, welcher eine Datei in folgendem
Format ausgibt:

"<dateiname>_decode.txt"

Diese Datei ist im Klartext sichtbar und nicht verschlüsselt.

==========================================[Encode (Selected Settings)]==========================================
Zum Codieren mit einem gewünschten Key / gewünschten Einstellungen kann dieser Vorgang gewählt werden.
Hierbei wird nach den Einstellungen der 3 Rotoren gefragt. Die 3 Rotoren können gewählt werden, da es verschiedene
Rotoren zur einstellung gab.

Folgend können "Plugs" gewählt werden. Diese können mit dem Slider bis auf 10 Verbindungen (=20 Plugs) erhöht werden.
Wenn der Plug A mit B verbunden ist, ergibt die Eingabe von A vor den Rotoren den Buchstaben B, folgend ergibt B auch A.
Nach den Rotoren wird B und A wieder getauscht, somit werden die Plugs vor und nach den Rotoren verwendet.

Wenn folgend eine .txt Datei gewählt wird kann diese mit dem "Run" Button codiert werden. Diese Datei ist in folgendem
Format sichtbar:

"<dateiname>_encode.enigma"

Diese Datei ist verschlüsselt und somit nicht im Klartext.

Der Schlüssel ist im folgenden Format sichtbar:

"<dateiname>.key"

==========================================[Encode (Random Key)]==========================================
Zum Codieren mit einem zufällig gewählten Schlüssel kann dieser Vorgang gewählt werden. In diesem Vorgang ist lediglich
die zu codierende Datei nötig, welche dann mit dem "Run" Button verschlüsselt wird. Diese Datei ist in folgendem
Format sichtbar:

"<dateiname>_encode.enigma"

Diese Datei ist verschlüsselt und somit nicht im Klartext.

Der Schlüssel ist im folgenden Format sichtbar:

"<dateiname>.key"

==========================================[Projekt]==========================================
Das Projekt lässt sich über folgende Links aufrufen und über den SSH oder HTTPS link clonen.

Project: https://gitlab.com/JP-DHBW/enigma

SSH: git@gitlab.com:JP-DHBW/enigma.git

HTTPS: https://gitlab.com/JP-DHBW/enigma.git

==========================================[Quelleninformation]==========================================
Die Rotoren sind Original übernommen worden von:
	- Enigma I
	- M3 Army
	- M3 & M4 Naval

Der Reflector ist von der original Enigma Model A.

==========================================[Projekt-Team]==========================================
Joey-Pascal Stein - Student an der DHBW Ravensburg (Campus Friedrichshafen), Kurs: TIM18
E-Mail: joey.stein@gmx.net

Stefan Reichmann - Student an der DHBW Ravensburg (Campus Friedrichshafen), Kurs: TIM18
E-Mail: stefan02.reichmann@gmail.com

==========================================[Selbstverständlichkeit]==========================================
Hiermit bestätigen wir, dass dieses Projekt aus eigener Hand (unter verwendung von Qt) entsanden ist, und keine
Vorlagen/Lösungen von extern verwendet haben.
Die Daten der Rotoren/Reflectoren der Enigma wurden aus Originalkopien im Netz übernommen.

Joey-Pascal Stein, Stefan Reichmann am 14.06.2019 in Friedrichshafen.