#include "enigma.h"

void Machine::encode(){
    //Einstiegsmethode für den Encode-Prozess

    Output::Logger::log("Starting Encode of filePath:");
    Output::Logger::log(this->filePath);

    string data = Machine::readFile(this->filePath); //Datei auslesen
    this->filePath = Machine::removeFileExtension(this->filePath);

    string returnData = "";
    for (int i = 0; i < this->fileLength; i++){
        char tmpChar = data[i];
        bool toLower = false;
        QRegExp alphabet("[A-Za-z]");
        if (alphabet.exactMatch(QString(tmpChar))){
            if (Machine::isLowerCase(tmpChar)){
                toLower = true;
            }
            tmpChar = toupper(tmpChar);
            //======================[PLUG - 1]======================
            tmpChar = this->runPlugs(tmpChar);

            //======================[ROTOREN]======================
            tmpChar = this->runRotors(tmpChar, 0);
            this->rotate(i);
            //======================[PLUG - 2]======================
            tmpChar = this->runPlugs(tmpChar);

            if (toLower){
                tmpChar = tolower(char(tmpChar));
            }
        }
        returnData += tmpChar;
    }
    Machine::writeToFile(this->filePath + "_encode.enigma", returnData);
    this->createKey();

    Output::Logger::log("Finished encode, Key available. Outputfile:");
    Output::Logger::log(this->filePath + "_encode.enigma");
    Output::Logger::log("Keyfile:");
    Output::Logger::log(this->filePath + ".key");
  }
