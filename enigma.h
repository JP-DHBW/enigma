#ifndef ENIGMA_H
#define ENIGMA_H
#include "./Logger/logger.h"
#include <QMainWindow>
#include <fstream>
#include <sstream>
#include "logger.h"
#include <ctime>
#include <cstdlib>
#include <array>

using namespace std;


namespace Enigma {
  class Machine;
}

class Machine{

public:
  Machine();
  Machine(string inputKey, string inputFilePath);
  Machine(string inputFilePath, int inputPlugs[10][2],int inputRotors[3]);
  Machine(string inputFilePath);
  ~Machine();

  //Hauptfunktionen
  void decode();
  void encode();

  //Nebenfunktionen
  //================|fileManager|================

  void writeToFile(string filePath, string data); //Schreibt in eine Datei

  string readFile(string filePath); //Liest Datei und gibt inhalt zurück

  string removeFileExtension(string filePath); //Entfernt Dateikürzel

  //================|plugs|================

  bool isNotPlugged(int nmbr);

  char runPlugs(char letter);

  //================|rotors|================

  char runRotors(char letter, int state);

  char rotateLetter(int nmb, char letter);

  void rotate(int i); //Funktion um einen Rotor zu rotieren

  void createRandomRotPos();

  //================|reverseRotor|================
  char rotateReverseLetter(int nmb, char letter);

  char reverseRotor(char letter, int state);

  int getRotorPosition(char letter, int rotor); //Rotor 8: reflector

  //================|Keys|================

  void createRandomKey();

  void createKey();

  void loadKey();

  //================|Letters/Alphabet|================

  char getLetter(int letnmb);

  int getAlphabetPosition(char letter);

  //================|Enigma.cpp|================

  void setArrays(int inputPlugs[10][2],int inputRotors[3]);

  void setZeroOffset();

  bool isLowerCase(char letter);

private:
  string filePath;
  string key;
  int fileLength;
  int posRot1;
  int posRot2;
  int posRot3;
  int plugs[10][2];
  int rotors[3];
  static const char * alphabet;
  static const char * rotor_0;
  static const char * rotor_1;
  static const char * rotor_2;
  static const char * rotor_3;
  static const char * rotor_4;
  static const char * rotor_5;
  static const char * rotor_6;
  static const char * rotor_7;
  static const char * reflector;
};
#endif
