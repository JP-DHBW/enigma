#include "gui.h"
#include "ui_gui.h"
#include <QFileDialog>
#include <QDir>
#include <QDebug>
#include <QListView>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QMessageBox>
#include <enigma.h>

Gui::Gui(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Gui)
{
    ui->setupUi(this);
    emit on_codingSet_currentIndexChanged(0);
}

Gui::~Gui()
{
    delete ui;
}

void Gui::on_fileSelect_clicked()
{
    QString filePath;
    if (ui->codingSet->currentIndex()==0){
        filePath = QFileDialog::getOpenFileName(this,"Open a Enigmafile",QDir::homePath(),tr("ENIGMA (*.enigma);; TXT (*.txt)"));
    } else {
        filePath = QFileDialog::getOpenFileName(this,"Open a Textfile",QDir::homePath(),tr("TXT(*.txt)"));
    }
    Gui::changeSelectedFileTitle(filePath);
}

void Gui::changeSelectedFileTitle(const QString filePath)
{
    ui->fileSelected->setText(filePath);
}

void Gui::on_quitBtn_clicked()
{
    close();
}

bool compareLabelPointer(QLabel* ptr1, QLabel* ptr2){
    if (ptr1 && ptr2){
        return ptr1->objectName() < ptr2->objectName();
    }
    return false;
}

bool compareBoxPointer(QComboBox* ptr1, QComboBox* ptr2){
    if (ptr1 && ptr2){
        return ptr1->objectName() < ptr2->objectName();
    }
    return false;
}

void Gui::on_plugAmt_valueChanged(int value)
{
    QRegExp labelEx("plugLabel*");
    labelEx.setPatternSyntax(QRegExp::Wildcard);
    QRegExp boxEx("plugSelect*");
    boxEx.setPatternSyntax(QRegExp::Wildcard);

    QList<QLabel*> labelList;
    foreach (QLabel* label, this->findChildren<QLabel*>()) {
        if (labelEx.exactMatch(label->objectName())){
            labelList += label;
        }
    }
    qSort(labelList.begin(), labelList.end(),compareLabelPointer);

    QList<QComboBox*> boxList;
    foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
        if (boxEx.exactMatch(box->objectName())){
            boxList += box;
        }
    }
    qSort(boxList.begin(),boxList.end(),compareBoxPointer);

    foreach (QLabel* label, labelList){
        label->setVisible(false);
        label->setEnabled(false);
    }

    foreach (QComboBox* box, boxList){
        box->setVisible(false);
        box->setEnabled(false);
    }

    for (int i = 0;i<value;i++) {
        labelList[i]->setVisible(true);
        labelList[i]->setEnabled(true);
        boxList[(i*2)]->setVisible(true);
        boxList[(i*2)]->setEnabled(true);
        boxList[(i*2+1)]->setVisible(true);
        boxList[(i*2+1)]->setEnabled(true);
    }
}

void Gui::on_codingSet_currentIndexChanged(int index)
{
    foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
        box->hide();
        box->setEnabled(false);
    }
    foreach (QLabel* label, this->findChildren<QLabel*>()) {
        label->hide();
        label->setEnabled(false);
    }
    ui->plugAmt->hide();
    ui->plugAmt->setEnabled(false);
    ui->keyFileSelect->hide();
    ui->keyFileSelect->setEnabled(false);
    ui->progressBar->hide();
    ui->progressBar->setEnabled(false);
    if(index==0){
        ui->codingSet->show();
        ui->codingSet->setEnabled(true);
        ui->codingSettings->show();
        ui->codingSettings->setEnabled(true);
        ui->fileSelect->show();
        ui->fileSelect->setEnabled(true);
        ui->fileLabel->show();
        ui->fileLabel->setEnabled(true);
        ui->fileSelected->show();
        ui->fileSelected->setEnabled(true);
        ui->runBtn->show();
        ui->runBtn->setEnabled(true);
        ui->quitBtn->show();
        ui->quitBtn->setEnabled(true);
        //ui->progressBar->show();
        //ui->progressBar->setEnabled(true);
        ui->keyFileSelected->show();
        ui->keyFileSelected->setEnabled(true);
        ui->keyFileLabel->show();
        ui->keyFileLabel->setEnabled(true);
        ui->keyFileSelect->show();
        ui->keyFileSelect->setEnabled(true);

    }
    if(index==1){
        foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
            QRegExp plugEx("plugSelect*");
            plugEx.setPatternSyntax(QRegExp::Wildcard);
            QList<QComboBox*> plugList;
            foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
                if (plugEx.exactMatch(box->objectName())){
                    plugList += box;
                }
            }
            foreach (QComboBox* box, plugList){
                box->setCurrentIndex(26);
            }
            QRegExp rotEx("rot*");
            rotEx.setPatternSyntax(QRegExp::Wildcard);
            QList<QComboBox*> rotList;
            foreach (QComboBox* box, this->findChildren<QComboBox*>()){
                if (rotEx.exactMatch(box->objectName())){
                    rotList += box;
                }
            }
            foreach (QComboBox* box, rotList){
                box->setCurrentIndex(8);
                QListView* view = qobject_cast<QListView *>(box->view());
                QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
                Q_ASSERT(view != nullptr);
                Q_ASSERT(model != nullptr);
                for (int i = 0;i<=8;i++) {
                    view->setRowHidden(i,false);
                    QStandardItem* item = model->item(i);
                    item->setFlags(item->flags() | Qt::ItemIsEnabled);
                }
            }
            box->show();
            box->setEnabled(true);
        }
        foreach (QLabel* label, this->findChildren<QLabel*>()) {
            label->show();
            label->setEnabled(true);
        }
        ui->plugAmt->show();
        ui->plugAmt->setEnabled(true);
        ui->plugAmt->setValue(0);
        emit on_plugAmt_valueChanged(0);
        ui->keyFileSelect->hide();
        ui->keyFileSelect->setEnabled(false);
        ui->keyFileLabel->hide();
        ui->keyFileLabel->setEnabled(false);
    }
    if(index==2){
        ui->fileSelect->show();
        ui->fileSelect->setEnabled(true);
        ui->fileLabel->show();
        ui->fileLabel->setEnabled(true);
        ui->fileSelected->show();
        ui->fileSelected->setEnabled(true);
        ui->runBtn->show();
        ui->runBtn->setEnabled(true);
        ui->quitBtn->show();
        ui->quitBtn->setEnabled(true);
        //ui->progressBar->show();
        //ui->progressBar->setEnabled(true);
        ui->codingSet->show();
        ui->codingSet->setEnabled(true);
        ui->codingSettings->show();
        ui->codingSettings->setEnabled(true);
    }
}


bool Gui::checkInputFile(int callType){

        QRegExp decodeEx("*.enigma");
        decodeEx.setPatternSyntax(QRegExp::Wildcard);
        QRegExp encodeEx("*.txt");
        encodeEx.setPatternSyntax(QRegExp::Wildcard);
        QRegExp keyEx("*.key");
        keyEx.setPatternSyntax(QRegExp::Wildcard);
        bool valid = false;
        if (callType == 0 && (decodeEx.exactMatch(ui->fileSelected->text()) | encodeEx.exactMatch(ui->fileSelected->text()))){
            valid = true;
        }
        if (callType==1 && encodeEx.exactMatch(ui->fileSelected->text())){
            valid = true;
        }
        if (callType==2 && encodeEx.exactMatch(ui->fileSelected->text())){
            valid = true;
        }
        if (!valid){
            QMessageBox *alertBox = new QMessageBox;
            alertBox->setText("No valid file selected.");

            alertBox->setStandardButtons(QMessageBox::Cancel);
            int reply = alertBox->exec();
            if(reply == QMessageBox::Cancel){
                return false;
            }
        }
        if (callType==0 && keyEx.exactMatch(ui->keyFileSelected->text())){
            valid = true;
        } else if (callType==0){
            valid = false;
        }
        if (!valid){
            QMessageBox *alertBox = new QMessageBox;
            alertBox->setText("No valid keyfile selected.");

            alertBox->setStandardButtons(QMessageBox::Cancel);
            int reply = alertBox->exec();
            if(reply == QMessageBox::Cancel){
                return false;
            }
        }
    return valid;
}

bool Gui::checkInput(){
    QList<QComboBox*> boxList;
    QList<QComboBox*> plugList;
    foreach(QComboBox* box, this->findChildren<QComboBox*>()){
        QRegExp boxEx("plugSelect*");
        boxEx.setPatternSyntax(QRegExp::Wildcard);
        if (!boxEx.exactMatch(box->objectName())){
            boxList += box;
        } else {
            plugList += box;
        }
    }

    qSort(plugList.begin(),plugList.end(),compareBoxPointer);
    if (!(ui->plugAmt->value()==0)){
        for (int i = 0; i < (ui->plugAmt->value())*2;i++) {
            boxList += plugList.takeAt(0);
        }
    }

    foreach (QComboBox* box, boxList){
        if (box->currentText()=="~"){
            QMessageBox *alertBox = new QMessageBox;
            alertBox->setText("There's some invalid user input");
            alertBox->setStandardButtons(QMessageBox::Cancel);
            int reply = alertBox->exec();
            if(reply == QMessageBox::Cancel){
                return false;
            }

        }
    }
    return true;
}

void Gui::on_runBtn_clicked()
{
    foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
        box->setEnabled(false);
    }
    foreach (QLabel* label, this->findChildren<QLabel*>()) {
        label->setEnabled(false);
    }
    ui->plugAmt->setEnabled(false);
    ui->keyFileSelect->setEnabled(false);
    ui->fileSelect->setEnabled(false);
    ui->quitBtn->setEnabled(false);
    ui->runBtn->setEnabled(false);

    int callType = ui->codingSet->currentIndex();

    //decode
    if (callType==0 && checkInputFile(callType)){
        string inputKey = ui->keyFileSelected->text().toLocal8Bit().constData();
        string inputPath = ui->fileSelected->text().toLocal8Bit().constData();
        Machine machine = Machine(inputKey,inputPath);        
        QMessageBox *alertBox = new QMessageBox;
        alertBox->setText("Job is done. Your file is within the same path as the source file.");
        alertBox->setStandardButtons(QMessageBox::Cancel);
        alertBox->exec();
    }
    //selectedKey

    if ( callType==1 && checkInput() && checkInputFile(callType)){
        string inputPath = ui->fileSelected->text().toLocal8Bit().constData();
        //inputPlugs[2][10];
        QRegExp boxEx("plugSelect*");
        boxEx.setPatternSyntax(QRegExp::Wildcard);
        int i = 0;
        int m = 0;
        foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
            if (boxEx.exactMatch(box->objectName())){
                this->plugs[m][i] = box->currentIndex();
                m++;
                if(m==1){
                    m = 0;
                    i++;
                }
            }
        }
        QRegExp rotorEx("rot*");
        rotorEx.setPatternSyntax(QRegExp::Wildcard);
        i = 0;
        foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
            if (rotorEx.exactMatch(box->objectName())){
                this->rotors[i] = box->currentIndex();
                i++;
            }
        }
        Machine machine = Machine(inputPath,this->plugs,this->rotors);
        QMessageBox *alertBox = new QMessageBox;
        alertBox->setText("Job is done. Your file is within the same path as the source file.");
        alertBox->setStandardButtons(QMessageBox::Cancel);
        alertBox->exec();
    }

    //rndKey
    if (callType==2 && checkInputFile(callType)){
        string inputPath = ui->fileSelected->text().toLocal8Bit().constData();
        Machine machine = Machine(inputPath);
        QMessageBox *alertBox = new QMessageBox;
        alertBox->setText("Job is done. Your file is within the same path as the source file.");
        alertBox->setStandardButtons(QMessageBox::Cancel);
        alertBox->exec();

    }

    foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
        box->setEnabled(true);
    }
    foreach (QLabel* label, this->findChildren<QLabel*>()) {
        label->setEnabled(true);
    }
    ui->plugAmt->setEnabled(true);
    ui->keyFileSelect->setEnabled(true);
    ui->fileSelect->setEnabled(true);
    ui->quitBtn->setEnabled(true);
    ui->runBtn->setEnabled(true);
}

void Gui::on_rot1_currentIndexChanged(int index)
{
    QList<QComboBox*> rotList;
    QRegExp rotorEx("rot*");
    rotorEx.setPatternSyntax(QRegExp::Wildcard);
    foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
        if (rotorEx.exactMatch(box->objectName())){
            rotList += box;
            QListView* view = qobject_cast<QListView *>(box->view());
            QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
            Q_ASSERT(view != nullptr);
            Q_ASSERT(model != nullptr);
            view->setRowHidden(index, true);
            QStandardItem* item = model->item(index);
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }
    }
    int indexList[3];
    int i = 0;
    foreach (QComboBox* box, rotList){
        indexList[i] = box->currentIndex();
        for (int m = 0;m<=8;m++) {
            QListView* view = qobject_cast<QListView *>(box->view());
            QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
            Q_ASSERT(view != nullptr);
            Q_ASSERT(model != nullptr);
            view->setRowHidden(m, false);
            QStandardItem* item = model->item(m);
            item->setFlags(item->flags() | Qt::ItemIsEnabled);
        }
        i++;
    }
    foreach (QComboBox* box, rotList){
        for (int i = 0;i<3;i++) {
            QListView* view = qobject_cast<QListView *>(box->view());
            QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
            Q_ASSERT(view!=nullptr);
            Q_ASSERT(model!=nullptr);
            view->setRowHidden(indexList[i], true);
            QStandardItem* item = model->item(indexList[i]);
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }
    }

}

void Gui::on_rot2_currentIndexChanged(int index)
{
    emit on_rot1_currentIndexChanged(index);
}

void Gui::on_rot3_currentIndexChanged(int index)
{
    emit on_rot1_currentIndexChanged(index);
}

void Gui::on_plugSelect11_currentIndexChanged(int index)
{
    QList<QComboBox*> plugList;
    QRegExp plugEx("plugSelect*");
    plugEx.setPatternSyntax(QRegExp::Wildcard);
    foreach (QComboBox* box, this->findChildren<QComboBox*>()) {
        if (plugEx.exactMatch(box->objectName())){
            plugList += box;
            QListView* view = qobject_cast<QListView *>(box->view());
            QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
            Q_ASSERT(view != nullptr);
            Q_ASSERT(model != nullptr);
            view->setRowHidden(index, true);
            QStandardItem* item = model->item(index);
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }
    }
    int indexList[20];
    int i = 0;
    foreach (QComboBox* box, plugList){
        indexList[i] = box->currentIndex();
        for (int m = 0;m<=26;m++) {
            QListView* view = qobject_cast<QListView *>(box->view());
            QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
            Q_ASSERT(view != nullptr);
            Q_ASSERT(model != nullptr);
            view->setRowHidden(m, false);
            QStandardItem* item = model->item(m);
            item->setFlags(item->flags() | Qt::ItemIsEnabled);
        }
        i++;
    }
    foreach (QComboBox* box, plugList){
        for (int i = 0;i<20;i++) {
            QListView* view = qobject_cast<QListView *>(box->view());
            QStandardItemModel* model = qobject_cast<QStandardItemModel*>(box->model());
            Q_ASSERT(view!=nullptr);
            Q_ASSERT(model!=nullptr);
            view->setRowHidden(indexList[i], true);
            QStandardItem* item = model->item(indexList[i]);
            item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
        }
    }
}

void Gui::on_plugSelect12_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect21_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect22_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect31_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect32_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect41_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect42_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect51_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect52_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect61_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect62_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect71_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect72_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect81_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect82_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect91_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect92_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect93_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_plugSelect94_currentIndexChanged(int index)
{
    emit on_plugSelect11_currentIndexChanged(index);
}

void Gui::on_keyFileSelect_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(this,"Open a Keyfile",QDir::homePath(),"*.key");
    Gui::changeSelectedKeyFileTitle(filePath);
}

void Gui::changeSelectedKeyFileTitle(const QString filePath){
    ui->keyFileSelected->setText(filePath);
}


