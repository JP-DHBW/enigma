#include "enigma.h"

int searchInRotor(string rotor, char letter){
    int i = 0;
    while (true){
        if (rotor[i] == letter){
            return i;
        }
        else if(i >= 26){
            return NULL;
        }
        i++;
    }
}


int Machine::getRotorPosition(char letter, int rotor){
    switch (rotor) {
    case 0:
        return searchInRotor(rotor_0, letter);
    case 1:
        return searchInRotor(rotor_1, letter);
    case 2:
        return searchInRotor(rotor_2, letter);
    case 3:
        return searchInRotor(rotor_3, letter);
    case 4:
        return searchInRotor(rotor_4, letter);
    case 5:
        return searchInRotor(rotor_5, letter);
    case 6:
        return searchInRotor(rotor_6, letter);
    case 7:
        return searchInRotor(rotor_7, letter);
    case 8:
        return searchInRotor(reflector, letter);
    default:
        break;
    }
}


char Machine::rotateReverseLetter(int nmb, char letter){
    int offset = 0;
    switch (nmb) {
    case 0:
        offset = this->posRot1;
        break;
    case 1:
        offset = this->posRot2;
        break;
    case 2:
        offset = this->posRot3;
        break;
    default:
        break;
    }
    int pos = 0;
    switch (this->rotors[nmb]) {
    case 0:
        pos = (searchInRotor(rotor_0, letter)-offset);
        break;
    case 1:
        pos = (searchInRotor(rotor_1, letter)-offset);
        break;
    case 2:
        pos = (searchInRotor(rotor_2, letter)-offset);
        break;
    case 3:
        pos = (searchInRotor(rotor_3, letter)-offset);
        break;
    case 4:
        pos = (searchInRotor(rotor_4, letter)-offset);
        break;
    case 5:
        pos = (searchInRotor(rotor_5, letter)-offset);
        break;
    case 6:
        pos = (searchInRotor(rotor_6, letter)-offset);
        break;
    case 7:
        pos = (searchInRotor(rotor_7, letter)-offset);
        break;
    case 8:
        pos = searchInRotor(reflector, letter);
        break;
    default:
        break;
    }

    //CheckNegative
    if (pos < 0){
        pos = (pos+26)%26;
    }
    else{
        pos = pos%26;
    }
    return alphabet[pos];
}


char Machine::reverseRotor(char letter, int state){
    state++;
    if (state >=4){
        if (state==8){
            return letter;
        }
        else if (state == 4){
            letter = alphabet[Machine::getRotorPosition(letter, 8)];
            return this->reverseRotor(letter, state);
        }
        else {
            letter = rotateReverseLetter(7-state, letter);
            return this->reverseRotor(letter, state);
        }
    }
    else{
        letter = rotateReverseLetter(state-1, letter);
        return this->reverseRotor(letter, state);
    }

}
