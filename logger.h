#ifndef _LOGGER_H_
#define _LOGGER_H_
using namespace std;
#include <string>

namespace Output{
  class Logger
  {
  public:
    static void log(string input);
    //Methode um Strings in Konsole auszugeben mit präfix "[INFO]"
    static void logError(string input);
    //Methode um Strings in Konsole auszugeben mit präfix "[INFO][ERROR]"
    static void logWarning(string input);
    //Methode um Strings in Konsole auszugeben mit präfix "[INFO][WARNING]"
    static void logDebug(string input);
    //Methode um Strings in Konsole auszugeben mit präfix "[INFO][DEBUG]"
  };
}

#endif
