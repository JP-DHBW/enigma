#include "enigma.h"
//Alphabet
const char * Machine::alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

//Rotoren (verkabelung)
const char * Machine::rotor_0 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
const char * Machine::rotor_1 = "AJDKSIRUXBLHWTMCQGZNPYFVOE";
const char * Machine::rotor_2 = "BDFHJLCPRTXVZNYEIWGAKMUSQO";
const char * Machine::rotor_3 = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
const char * Machine::rotor_4 = "VZBRGITYUPSDNHLXAWMJQOFECK";
const char * Machine::rotor_5 = "JPGVOUMFYQBENHZRDKASXLICTW";
const char * Machine::rotor_6 = "NZJHGRCXMYSWBOUFAIVLPEKQDT";
const char * Machine::rotor_7 = "FKQHTLXOCBJSPDZRAMEWNIUYGV";

const char * Machine::reflector = "EJMZALYXVBWFCRQUONTSPIKHGD";

int Machine::getAlphabetPosition(char letter){
    int i = 0;
    while (true){
        if (this->alphabet[i] == letter){
            return i;
        }
        if (i >=26){
            break;
        }
        i++;
    }
    return NULL;
}

char Machine::getLetter(int letnmb){
    return this->alphabet[letnmb%26];
}
